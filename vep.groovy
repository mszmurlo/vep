#!/usr/bin/env groovy

// vep - Vertx Empty Project generator
// Copyright (C) 2018 - mszmurlo@buguigny.org

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// See <http://www.gnu.org/licenses/>. 


/* Change log
 v0.2.1 on 2020.03.21: 
 added optional cli parameter '-vv <vertx version>'. defaults to 3.8.5
*/  


//RST = "\u001B[0m"
//RED = "\u001B[31m"
//REDB = "\u001B[31m\u001B[1m"
//BOLD = "\u001B[1m";

// def ANSI_GREEN = "\u001B[32m";
// def ANSI_YELLOW = "\u001B[33m";
// def ANSI_BOLD = "\u001B[1m";



// *** Prints an error
def error(message) { println("error: ${message}") }
// *** Prints an error in red and exits
// def error_e(message) { error(message); System.exit(-1); }


// *****
// ***** Creates the directory hierarchy
// *****
def create_directories(root, dirs) {
    dirs.each {
	def f = new File("${root}/${it}")
	println("   Creating directory ${f}")
	f.mkdirs()
    }
}


// *****
// ***** Creates the README.md file at the root of the project
// *****
def create_readme_md(params) {
println("   Creating file ${params.project_name}/README.md")
    def readme_md = '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style>
    body {
      background-color: #F0F0F0;
      color: #202020;
      margin-right : 10%;
      margin-left : 10%;
      padding : 0px;
      font-size : 12pt; 
      font-family: Georgia, Palatino, Times, 'Times New Roman', sans-serif;
      background-position : top left;
      background-repeat : no-repeat;
      text-align : justify;
    }
    </style>
</head>
<body>

# `${project_name}`

The is the README.md file for the project "${project_name}".

------

<small>
Initialized on ${new Date().format( 'yyyy/MM/dd' )} with 
`groovy vep.groovy ${cli}`

${vep_copyright}
</small>
</body></html>
'''
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(readme_md).make(params)
    new File( params.project_name, 'README.md' ).withWriterAppend { w -> w << txt }
}



// *****
// ***** Creates the .gitignore file at the root of the project
// *****
def create_dot_gitignore(params) {
println("   Creating file ${params.project_name}/.gitignore")
    def gitignore = '''## Project skeleton generated with ${vep_copyright}

target/
pom.xml.*
release.properties
dependency-reduced-pom.xml
buildNumber.properties
.mvn/timing.properties
*~
.vertx/

tmp/

## Those files are generated automatically with either markdown, plantuml or javadoc
*.png
*.html
doc/javadoc
'''
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(gitignore).make(params)
    new File( params.project_name, '.gitignore' ).withWriterAppend { w -> w << txt }
}
    

// *****
// ***** Creates the pom.xml file at the root of the project
// *****
def create_pom_xml(params) {
    println("   Creating file ${params.project_name}/pom.xml")

    if(params.main_verticle) {
	params.startup = """
<Main-Class>io.vertx.core.Launcher</Main-Class>
<Main-Verticle>${params.pkg_name}.${params.mclass}</Main-Verticle>
"""
    }
    else {
	params.startup = "<Main-Class>${params.pkg_name}.${params.mclass}</Main-Class>"
    }

    
    def pom_xml = '''<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

<!-- Project skeleton generated with ${vep_copyright} -->

  <modelVersion>4.0.0</modelVersion>

  <groupId>${pkg_name}</groupId>
  <artifactId>${project_name}</artifactId>
  <version>0.1</version>

  <properties>
    <vertx.version>${vertx_version}</vertx.version> 
    <skipTests>true</skipTests>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>io.vertx</groupId>
        <artifactId>vertx-stack-depchain</artifactId>
        <version>\\${vertx.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>io.vertx</groupId>
      <artifactId>vertx-core</artifactId>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.vertx</groupId>
      <artifactId>vertx-unit</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>io.vertx</groupId>
      <artifactId>vertx-web</artifactId>
<!--  
      <version>3.6.2</version>
-->
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-javadoc-plugin</artifactId>
      <version>3.0.1</version>
      <type>maven-plugin</type>
      <scope>test</scope>
    </dependency>

    <!-- Change here the dependency for another log lib -->
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.2.3</version>
    </dependency>

  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.5.1</version>
          <configuration>
            <source>1.8</source>
            <target>1.8</target>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>2.4.3</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <manifestEntries>

                  ${startup}

                  </manifestEntries>
                </transformer>
                <transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                  <resource>META-INF/services/io.vertx.core.spi.VerticleFactory</resource>
                </transformer>
              </transformers>
              <artifactSet>
              </artifactSet>
              <outputFile>\\${project.build.directory}/\\${project.artifactId}-\\${project.version}-fat.jar</outputFile>
            </configuration>
          </execution>
        </executions>
      </plugin>


    </plugins>
  </build>

</project>

'''
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(pom_xml).make(params)
    new File( params.project_name, 'pom.xml' ).withWriterAppend { w -> w << txt }
}


// *****
// ***** Creates the startup file
// *****
def create_main(params) {
    println("   Creating file ${params.project_name}/${params.main_src_dir}/${params.mclass}.java")
    params.log = "private static final Logger log = LoggerFactory.getLogger(${params.mclass}.class);"
    if(params.main_verticle) {
	params.p_1 = """
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
"""
	params.p_2 = "public class ${params.mclass} extends AbstractVerticle {"
	params.p_3 = """
  @Override
  public void start(Future<Void> sf) {
"""
	params.p_4 = "sf.complete();"
	params.p_5 = "sf.fail(ar.cause());"
    } // if()
    else {
	params.p_1 = """
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
"""
	params.p_2 = """ 
  public class ${params.mclass} {

    Vertx vertx = null;
"""
	params.p_3 ="""
  public static void main(String[] args) {
    (new ${params.mclass}()).startup(args);
  }

  private void startup(String[] args) {
     VertxOptions opts = new VertxOptions()
     // add some options with setXXX() methods
     ;
     vertx = Vertx.vertx(opts);
"""
	params.p_4 = ""
	params.p_5 = "System.exit(-1);"

    } // else
	
    def src = '''// Project skeleton generated with vep.
// ${vep_copyright}

package ${pkg_name};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;

${p_1}
${p_2}

  ${log}
${p_3}
    log.info("Starting HTTP server");

    HttpServer server = vertx.createHttpServer();  // Create the server
    Router router = Router.router(vertx);          // Define the router
    router.get("/:p").handler(this::rootHandler);  // 'GET /xxx' will be by the 'rootHandler' method
    router.route().handler(this::defHandler);      // Any other URL will be handeled by 'defHandler'
    server
      .requestHandler(router::accept)
      .listen(8080, ar -> {
        if(ar.succeeded()) {
          log.info("Server running on port 8080");
          ${p_4}
        }
        else {
          log.error("Could not start server on port 8080");
          ${p_5}
        }
      });
  }

  private void rootHandler(RoutingContext rc) {
    String parameter = rc.request().getParam("p");
    log.info("Got a request with parameter '{}'", parameter);
    rc.response()
      .putHeader("Content-Type", "text/text; charset=utf-8")
      .setStatusCode(200)
      .end("Sending back parameter '"+parameter+"'\\\\n");
  }

  private void defHandler(RoutingContext rc) {
    log.warn("Got a request '{} {}'. Not permitted.", rc.request().method(), rc.request().absoluteURI());
    rc.response()
      .setStatusCode(404)
      .end();
  }
}
'''
   
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(src).make(params)
    new File( "${params.project_name}/${params.main_src_dir}",  "${params.mclass}.java").withWriterAppend { w -> w << txt }
}



// *****
// ***** Creates the test verticle file
// *****
def create_main_unit_test(params) {
    println("   Creating file ${params.project_name}/${params.test_src_dir}/${params.mclass}Test.java")
    def src = '''// Project skeleton generated with vep.
// ${vep_copyright}

package ${pkg_name};

import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class ${mclass}Test {

  private Vertx vertx;

 @Before
  public void setUp(TestContext tc) {
    vertx = Vertx.vertx();
    vertx.deployVerticle(${mclass}.class.getName(), tc.asyncAssertSuccess());
  }

  @After
  public void tearDown(TestContext tc) {
    vertx.close(tc.asyncAssertSuccess());
  }

  @Test
  public void test200IfParamater(TestContext tc) {
    Async async = tc.async();
    vertx.createHttpClient().getNow(8080, "localhost", "/toto", response -> {
      tc.assertEquals(response.statusCode(), 200);
      response.bodyHandler(body -> {
        tc.assertTrue(body.length() > 0);
        async.complete();
      });
    });
  }
  
  @Test
  public void test404IfNoParamater(TestContext tc) {
    Async async = tc.async();
    vertx.createHttpClient().getNow(8080, "localhost", "/", response -> {
      tc.assertEquals(response.statusCode(), 404);
      response.bodyHandler(body -> {
        tc.assertTrue(body.length() == 0);
        async.complete();
      });
    });
  }
}
'''
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(src).make(params)
    new File( "${params.project_name}/${params.test_src_dir}",  "${params.mclass}Test.java").withWriterAppend { w -> w << txt }
}


// *****
// ***** Creates the log config file file
// *****
def create_log_config(params) {
    println("   Creating file ${params.project_name}/${params.resources_dir}/logback.xml")
    def cnf = '''<!-- Project skeleton generated with ${vep_copyright} -->

<configuration>
  
  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
    <encoder>
      <pattern>%d{HH:mm:ss.SSS} %highlight(%-5level %logger{50}) - %msg%n</pattern>
    </encoder>
  </appender>

  <logger name="com.mchange.v2" level="warn"/>
  <logger name="io.netty" level="warn"/>
  <logger name="io.vertx" level="info"/>
  <logger name="${pkg_name}" level="debug"/>
  
  <root level="debug">
    <appender-ref ref="STDOUT"/>
  </root>
  
</configuration>
'''
    def te = new groovy.text.SimpleTemplateEngine()
    def txt = te.createTemplate(cnf).make(params)
    new File( "${params.project_name}/${params.resources_dir}",  "logback.xml").withWriterAppend { w -> w << txt }
}
    
// *************************************************************************
// *** Start point of the script
// *************************************************************************

// *** Parameters of the project to be populated from the analysis of
// the command line
def parameters = [
    "vep_version" : "vep v0.2.1 (2020.03.21)",
    "vep_author"  : "mszmurlo@buguigny.org",
    "vep_url"     : "https://gitlab.com/mszmurlo/vep",
    // doc :      will contain the documentations, generated api, etc
    // src :      the source files
    // tmp :      will contain temporary files, experiments
    // tests :    various test files
    "directories" : [
	"doc", "doc/javadoc",
	"src", "src/main/java", "src/main/resources", "src/test/java/",
	"tmp",
	"tests"
    ],
    "resources_dir" : "src/main/resources"
]
parameters.vep_copyright = "${parameters.vep_version} - See ${parameters.vep_url} - MIT License"
println(parameters.vep_copyright)

// *** Get the parameters from the command line
def cli = new CliBuilder(usage: "vep -pn <project_name> -pkg <full.pkg.herarchy> [-mclass <MainVerticle>]")
cli.width = 200
cli.mclass(args:1, argName:'Main', 'if present, name of the startup class and \'Main\' otherwise')
cli.mfunc('if present, generate a main() function in the \'mc\'-class. If omitted, \'mc\'-class will be a verticle')
cli.pkg(args:1, argName:'full.pkg.herarchy', 'package of the project')
cli.pn(args:1, argName:'project_name', 'name of the project and of the working directory')
cli.vv(args:1, argName:'vertx_version', 'if present, sets vert.x version. Defaults to 3.8.5')

if(args.size() == 0) {
    error("No command line parameters")
    cli.usage()
    System.exit(-1)
}

def opt = cli.parse(args)
if(!opt) {
    System.exit(-1)
}

if(!opt.pn) {
    error("Missing mandatory '-pn' option")
    cli.usage()
    System.exit(-1)
}
else {
    parameters.project_name = opt.pn
    parameters.cli="-pn ${opt.pn}"
}

if(!opt.vv) {
    parameters.vertx_version = "3.8.5"
}
else {
    parameters.vertx_version = opt.vv
    parameters.cli += " -vv ${opt.vv}"
}

if(!opt.pkg) {
    error("Missing mandatory '-pkg' option")
    cli.usage()
    System.exit(-1)
}
else {
    parameters.pkg_name = opt.pkg
    parameters.cli += " -pkg ${opt.pkg}"
    def src = opt.pkg.replace(".", "/")
    
    def main_src = "src/main/java/${src}"
    parameters.directories.add(main_src)
    parameters.main_src_dir = main_src
    
    def test_src = "src/test/java/${src}"
    parameters.directories.add(test_src)
    parameters.test_src_dir = test_src
}

if(opt.mclass) {
    parameters.mclass = opt.mclass[0].toUpperCase()+opt.mclass[1..-1]
    parameters.cli += " -mclass ${opt.mclass}"
}
else {
    parameters.mclass = "Main"
}

if(opt.mfunc) {
    parameters.cli += " -mfunc"
}
parameters.main_verticle = !opt.mfunc

// generate

println("Will generate a startup with ${parameters.main_verticle ? 'a Verticle' : 'a main() function'}")
println("Creating directories below {parameters.project_name}")
create_directories(parameters.project_name, parameters.directories)

println("Creating files ${parameters.project_name}/README.md")
create_readme_md(parameters)
create_dot_gitignore(parameters)
create_pom_xml(parameters)
create_main(parameters)
create_main_unit_test(parameters)
create_log_config(parameters)


