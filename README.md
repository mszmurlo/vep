<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>

# `vep` Vertx Empty Project generator

`vep` (Vertx Empty Project) is a quick and dirty script written in
Groovy that generates a compilable and *unit-testable* Eclipse Vertx
project. [Eclipse Vertx](https://vertx.io) is a tool-kit for building
reactive applications on the JVM.

Current version is v0.2.1.


## Objectives

If you want to start a new Vertx project, you can either create all
the project structure by hand and write the `pom.xml` file (if you
want to use Maven); or you can copy an older project, strip it down to
the bare minimum and search-replace all class and directory names; or
you can clone the Vertx maven starter Github repository `git clone
https://github.com/vert-x3/vertx-maven-starter.git vt` and, as
previously, search-replace all class and directory names; or you can
use the [starter page](https://start.vertx.io/) on Vert.x web site but
it doesn't generate `main()` function. Probably there are plugins for
Maven (or for whatever other build tool one is using) able to generate
empty projects but I'm aware of none on the command line. I was
pointed on [vertx-starter](https://vertx-starter.jetdrone.xyz/) which
works online and which is able to generate code for several languages.

Or you can use `vep` to generate for you the skeleton of a new project
in less than 10 seconds.

NB: what `vep` generates is highly inspired from the [Vertx 3 Maven
starter](https://github.com/vert-x3/vertx-maven-starter).


`vep` generates the following:

* the directory tree for the whole project to host source code, unit
  test code, documentation, executables (fat-jars), etc..
* the `pom.xml` file for building with Maven with all dependencies
  (vertx, logging, etc).
* the source code of a dummy HTTP server listening on port 8080 and
  the unit test of this server.
* the logger configuration file, the dependency in the `pom.xml` file
  and the includes in the source of the startup file.
* some additional directories for the documentations, tests scripts other
  than the unit tests, etc
* a `.gitignore` file


## Installation

Download the file `vep.groovy` and execute it with `groovy vep.groovy
<options>`.

Under *nix, if you want to call `vep` rather than `groovy vep.groovy ... `:

* Make it an executable: `chmod +x vep.groovy`.
* Copy `vep.groovy` as `vep` in a directory on the `PATH`, as for
  exemple`copy vep.groovy $HOME/usr/bin/vep` or `copy vep.groovy
  /usr/local/bin/vep` (you need to be root).
* Execute it with `vep <options>`

That's it.

## Generating and running the skeleton

1. Generate the skeleton : `./vep.groovy -pn myProject -pkg
  org.myorg.myproject`. See command line options below.
2. Get into the generated directory: `cd myProject`.
3. Build the executable : `mvn package`
1. Run the executable `java -jar target/myProject-0.1-fat.jar`
1. Test it: `curl  http://localhost:8080/hello-world`


## Command line options 

`vep` currently supports the following options on its command line:

* `-pn <project_name>`. Mandatory.  
  Specifies the name of the project. That will be the
  name of the project's directory as well as the name of the executable
  jars.
  
* `-pkg <package hierarchy>`. Mandatory.  
  Specifies the package name of the startup
  class and, thus, the names of the directories in the source
  hierarchy. For example if one provides `-pkg org.mycompany.myproject`,
  the root package will be `org.mycompany.myproject` and the source code
  directory will be `src/main/java/org/mycompany/myproject`.
  
* `-mclass <name of the main class>`. Optional.  
  Specifies the name of the startup class, the class which holds the
  startup code. That will  either be a verticle that inherits from
  `AbstractVerticle` or a regular class. In either case it contains a
  basic HTTP server that listens on port 8080. Notice that the name
  that is provided will be capitalized. For example `-mclass
  myverticle` will result in the generation of the file
  `Myverticle.java` which defines the class `Myverticle`.  If omitted,
  the name of the startup class will be `Main`.

* `-mfunc`. Optionnal.  
  If present, the startup class defined with the `-mclass` option will
  be a regular class and a `main()` function will be
  generated. Otherwise the startup class will be a verticle and a
  `start()` method will be generated.
  
* `-vv <vertx version>`. Optional.  
  If present, requires the version `<vertx version>` for
  Vert.x. Otherwise version `3.8.5` will be required.


## Change log

### From v0.2 to v0.2.1

* added optional cli parameter `-vv <vertx version>`. defaults to
  `3.8.5`.

### From v0.1 to v0.2

* `-log` from version 0.1 has been suppressed.  `vep` now generates
  dependencies, configuration file and generates logging code
  systematically.
* `mclass` replaces `-main` from version 0.1. 
* Added `-mfunc` option to generate a regular class with a `main()`
startup function rather than a verticle with  `start()`.


## Examples

### The minimum command line:

To create a project named `myProject` with a package hierarchy being
`org.myorg.myproject` type.

	$ vep -pn myProject -pkg org.myorg.myproject
    vep v0.2 (2019.03.03) - See https://gitlab.com/mszmurlo/vep - MIT License
    Will generate a startup with a Verticle
    Creating directories below {parameters.project_name}
       Creating directory myProject/doc
       Creating directory myProject/doc/javadoc
       Creating directory myProject/src
       Creating directory myProject/src/main/java
       Creating directory myProject/src/main/resources
       Creating directory myProject/src/test/java
       Creating directory myProject/tmp
       Creating directory myProject/tests
       Creating directory myProject/src/main/java/org/myorg/myproject
       Creating directory myProject/src/test/java/org/myorg/myproject
    Creating files myProject/README.md
       Creating file myProject/README.md
       Creating file myProject/.gitignore
       Creating file myProject/pom.xml
       Creating file myProject/src/main/java/org/myorg/myproject/Main.java
       Creating file myProject/src/test/java/org/myorg/myproject/MainTest.java
       Creating file myProject/src/main/resources/logback.xml


The directory hierarchy that `vep` generated is:

    myProject/
    ├── doc
    │   └── javadoc
    ├── pom.xml
    ├── README.md
    ├── src
    │   ├── main
    │   │   ├── java
    │   │   │   └── org
    │   │   │       └── myorg
    │   │   │           └── myproject
    │   │   │               └── Main.java
    │   │   └── resources
    │   │       └── logback.xml
    │   └── test
    │       └── java
    │           └── org
    │               └── myorg
    │                   └── myproject
    │                       └── MainTest.java
    ├── tests
    └── tmp


Please notice that:

* `doc/javadoc` will be ignored by Git
* the content of the directory `src/main/resources` will be included
  in the fatjar. This is the place for the internal configuration
  files, web templates, etc
* `tests` is for whatever test scripts other than unit tests you may want to write.
* `tmp` is for whatever can be deleted by error
* `pom.xml` contains the line `<skipTests>true</skipTests>` in the
  `properties` section to speedup the build. If you want to perform
  the tests replace `true` by `false`.
  

### The command line for generating a regular class

To create a project named `myProject`, with a package hierarchy being
`org.myorg.myproject`, with a regular startup class holding a `main()`
function:

    $ ./vep.groovy -pn myProject -pkg org.myorg.myproject -mclass project -mfunc
    vep v0.2 (2019.03.03) - See https://gitlab.com/mszmurlo/vep - MIT License
    Will generate a startup with a main() function
    Creating directories below {parameters.project_name}
       Creating directory myProject/doc
       Creating directory myProject/doc/javadoc
       Creating directory myProject/src
       Creating directory myProject/src/main/java
       Creating directory myProject/src/main/resources
       Creating directory myProject/src/test/java
       Creating directory myProject/tmp
       Creating directory myProject/tests
       Creating directory myProject/src/main/java/org/myorg/myproject
       Creating directory myProject/src/test/java/org/myorg/myproject
    Creating files myProject/README.md
       Creating file myProject/README.md
       Creating file myProject/.gitignore
       Creating file myProject/pom.xml
       Creating file myProject/src/main/java/org/myorg/myproject/Project.java
       Creating file myProject/src/test/java/org/myorg/myproject/ProjectTest.java
       Creating file myProject/src/main/resources/logback.xml
    


The directory hierarchy that `vep` generated is:

    myProject/
    ├── doc
    │   └── javadoc
    ├── pom.xml
    ├── README.md
    ├── src
    │   ├── main
    │   │   ├── java
    │   │   │   └── org
    │   │   │       └── myorg
    │   │   │           └── myproject
    │   │   │               └── Project.java
    │   │   └── resources
    │   │       └── logback.xml
    │   └── test
    │       └── java
    │           └── org
    │               └── myorg
    │                   └── myproject
    │                       └── ProjectTest.java
    ├── tests
    └── tmp


## Roadmap (no promise on dates)

* Allow specification of the build tool (Gradle and Ivy) and generate
  build files accordingly.
* Add more option for skeletons:
  * A Vertx configuration file and usage example in startup code
  * Inclusion of Javadoc plugin as an option
  * Unit tests as an option
  * Add cluster lib selection and code snippet.
* change cli parameter names, mainly `-mclass` and `-mfunc`

Any other idea are welcome.

## License

`vep` is released under the MIT License.
